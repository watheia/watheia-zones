#!/usr/bin/env bash

# Install headless chrome for testing
sudo apt-get update && sudo apt-get install -y \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg \
  --no-install-recommends \
  && curl -sSL https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add - \
  && sudo echo "deb https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list \
  && sudo apt-get update && \
     sudo apt-get install -y \
        google-chrome-stable \
        fontconfig \
        fonts-ipafont-gothic \
        fonts-wqy-zenhei \
        fonts-thai-tlwg \
        fonts-kacst \
        fonts-symbola \
        fonts-noto \
        fonts-freefont-ttf \
        --no-install-recommends \
  && sudo apt-get purge --auto-remove -y curl gnupg \
  && sudo rm -rf /var/lib/apt/lists/*
