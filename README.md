[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/watheia/watheia-zones)

# Using multiple zones

With Next.js you can use multiple apps as a single app using its
[multi-zones feature](https://nextjs.org/docs/advanced-features/multi-zones). This is an example
showing how to use it.

- All pages should be unique across zones. For example, the `home` app should not have a
  `pages/blog/index.js` page.
- The `home` app is the main app and therefore it includes the rewrites that map to the `blog`
  app in [next.config.js](home/next.config.js)
- The `blog` app sets
  [`basePath`](https://nextjs.org/docs/api-reference/next.config.js/basepath) to `/blog` so that
  generated pages, Next.js assets and public assets are within the `/blog` subfolder.

## Content Model

```plantuml
@startyaml
buildCommand: "yarn build"
dataDir: content/data
logicFields:
  - blog_feed_section.recent_count
  - blog_feed_section.background_image_opacity
  - features_section.background_image_opacity
  - hero_section.background_image_opacity
  - grid_section.background_image_opacity
  - cta_section.background_image_opacity
  - form_section.background_image_opacity
models:
  action:
    fields:
      -
        label: Label
        name: label
        required: true
        type: string
      -
        default: "#"
        label: URL
        name: url
        required: true
        type: string
      -
        default: link
        label: Style
        name: style
        options:
          - link
          - primary
          - secondary
        required: true
        type: enum
      -
        default: false
        label: "Show icon"
        name: has_icon
        type: boolean
      -
        label: Icon
        name: icon
        options:
          - arrow-left
          - arrow-right
          - facebook
          - github
          - instagram
          - linkedin
          - twitter
          - vimeo
          - youtube
        type: enum
      -
        default: right
        description: "Select \"center\" to display icon only and hide the label"
        label: "Icon position"
        name: icon_position
        options:
          - left
          - right
          - center
        type: enum
      -
        default: false
        label: "Open in new tab/window"
        name: new_window
        type: boolean
      -
        default: false
        description: "Add rel=\"nofollow\" attribute to the link"
        label: "No follow"
        name: no_follow
        type: boolean
    label: Action
    labelField: label
    type: object
  advanced:
    fields:
      -
        description: "The title of the page"
        label: Title
        name: title
        required: true
        type: string
      -
        description: "Page sections"
        items:
          models:
            - hero_section
            - features_section
            - grid_section
            - blog_feed_section
            - cta_section
            - form_section
          type: model
        label: Sections
        name: sections
        type: list
      -
        models:
          - stackbit_page_meta
        name: seo
        type: model
    hideContent: true
    label: "Advanced page"
    layout: advanced
    type: page
  blog_feed_section:
    fields:
      -
        description: "The title of the section"
        label: Title
        name: title
        type: string
      -
        description: "The subtitle of the section displayed above the title"
        label: Subtitle
        name: subtitle
        type: string
      -
        description: "Action buttons displayed below blog posts"
        items:
          models:
            - action
          type: model
        label: "Action buttons"
        name: actions
        type: list
      -
        default: three
        description: "Show the specified number of posts in a blog feed grid row"
        label: "Blog feed columns"
        name: blog_feed_cols
        options:
          - two
          - three
        type: enum
      -
        default: false
        description: "Display posts as cards, i.e. add border, background and some padding around post content"
        label: "Enable cards"
        name: enable_cards
        type: boolean
      -
        default: false
        description: "Show the specified number of recent posts. Should not be used with author, category and tag filters"
        label: "Show recent posts only"
        name: show_recent
        required: true
        type: boolean
      -
        label: "Number of recent posts to show"
        name: recent_count
        type: number
      -
        description: "Filter posts by an author"
        label: "Author filter"
        models:
          - person
        name: author
        type: reference
      -
        description: "Filter posts by a category"
        label: "Category filter"
        models:
          - category
        name: category
        type: reference
      -
        description: "Filter posts by a tag"
        label: "Tag filter"
        models:
          - tag
        name: tag
        type: reference
      -
        default: true
        description: "Show the publish date of the post"
        label: "Show date"
        name: show_date
        type: boolean
      -
        default: false
        description: "Show the categories of the post"
        label: "Show categories"
        name: show_categories
        type: boolean
      -
        default: false
        description: "Show the author of the post"
        label: "Show author"
        name: show_author
        type: boolean
      -
        default: false
        description: "Show the excerpt of the post"
        label: "Show excerpt"
        name: show_excerpt
        type: boolean
      -
        default: true
        description: "Show the post image thumbnail"
        label: "Show image"
        name: show_image
        type: boolean
      -
        default: center
        description: "The horizontal alignment of the section content (title, subtitle, action buttons)"
        label: "Section alignment"
        name: align
        options:
          - left
          - right
          - center
        type: enum
      -
        default: medium
        description: "The padding area (space) on the top of the section"
        label: "Section top padding"
        name: padding_top
        options:
          - none
          - small
          - medium
          - large
        type: enum
      -
        default: medium
        description: "The padding area (space) on the bottom of the section"
        label: "Section bottom padding"
        name: padding_bottom
        options:
          - none
          - small
          - medium
          - large
        type: enum
      -
        default: false
        description: "Add section bottom border"
        label: "Section border"
        name: has_border
        type: boolean
      -
        default: none
        description: "The background color of the section"
        label: "Background color"
        name: background_color
        options:
          - none
          - primary
          - secondary
        type: enum
      -
        description: "The image displayed in the background of the section"
        label: "Background image"
        name: background_image
        type: image
      -
        description: "An integer between 0 and 100. A lower value makes the image more transparent"
        label: "Background image opacity"
        name: background_image_opacity
        type: number
      -
        default: cover
        description: "The size of the background image"
        label: "Background image size"
        name: background_image_size
        options:
          - auto
          - contain
          - cover
        type: enum
      -
        default: "center center"
        description: "The starting position of a background image. The first value is the horizontal position, and the second value is the vertical"
        label: "Background image position"
        name: background_image_position
        options:
          - "left top"
          - "left center"
          - "left bottom"
          - "center top"
          - "center center"
          - "center bottom"
          - "right top"
          - "right center"
          - "right bottom"
        type: enum
      -
        default: no-repeat
        description: "Repeat the image to cover the whole area"
        label: "Background image repeat"
        name: background_image_repeat
        options:
          - repeat
          - no-repeat
        type: enum
    label: "Blog feed section"
    labelField: title
    type: object
  category:
    fields:
      -
        description: "A unique identifier used when filtering posts, e.g. \"demo\""
        label: ID
        name: id
        required: true
        type: string
      -
        description: "The link to the category page, e.g. \"blog/category/demo\""
        label: Link
        name: link
        type: string
      -
        description: "The title of the category"
        label: Title
        name: title
        required: true
        type: string
    folder: categories
    label: Category
    type: data
  config:
    fields:
      -
        description: "Site title"
        label: Title
        name: title
        required: true
        type: string
      -
        description: "The base URL of this site. Useful for sites hosted under specific path, e.g.: https://www.example.com/my-site/"
        hidden: true
        label: "Base URL"
        name: path_prefix
        required: true
        type: string
      -
        description: "The domain of your site, including the protocol, e.g. https://mysite.com/"
        label: Domain
        name: domain
        type: string
      -
        description: "A square icon that represents your website"
        label: Favicon
        name: favicon
        type: image
      -
        default: full-width
        description: "The layout of the site"
        label: Layout
        name: layout_type
        options:
          - full-width
          - boxed
        required: true
        type: enum
      -
        default: classic
        description: "The style of the site"
        label: Style
        name: style
        options:
          - minimal
          - classic
          - bold
        required: true
        type: enum
      -
        default: blue
        description: "The palette of the site"
        label: Palette
        name: palette
        options:
          - blue
          - green
          - red
          - gray
        required: true
        type: enum
      -
        default: dark
        description: "The color scheme of the site"
        label: Mode
        name: mode
        options:
          - light
          - dark
        required: true
        type: enum
      -
        default: sans-serif
        label: Font
        name: base_font
        options:
          - sans-serif
          - serif
        required: true
        type: enum
      -
        label: "Header configuration"
        models:
          - header
        name: header
        type: model
      -
        label: "Footer configuration"
        models:
          - footer
        name: footer
        type: model
    file: config.json
    label: "Site configuration"
    type: data
  cta_section:
    fields:
      -
        description: "The title of the section"
        label: Title
        name: title
        type: string
      -
        description: "The text content of the section"
        label: Content
        name: content
        type: markdown
      -
        items:
          models:
            - action
          type: model
        label: "Action buttons"
        name: actions
        type: list
      -
        default: bottom
        description: "The position of action buttons relative to the text content"
        label: "Action buttons position"
        name: actions_position
        options:
          - top
          - bottom
          - left
          - right
        type: enum
      -
        default: fourty
        description: "The action buttons container width as a percentage of the section width. Used only when the action buttons position is set to \"left\" or \"right\""
        label: "Action buttons width"
        name: actions_width
        options:
          - fourty
          - fifty
          - sixty
        type: enum
      -
        default: left
        description: "The horizontal alignment of the section content"
        label: "Section alignment"
        name: align
        options:
          - left
          - right
          - center
        type: enum
      -
        default: medium
        description: "The padding area (space) on the top of the section"
        label: "Section top padding"
        name: padding_top
        options:
          - none
          - small
          - medium
          - large
        type: enum
      -
        default: medium
        description: "The padding area (space) on the bottom of the section"
        label: "Section bottom padding"
        name: padding_bottom
        options:
          - none
          - small
          - medium
          - large
        type: enum
      -
        default: false
        description: "Add section bottom border"
        label: "Section border"
        name: has_border
        type: boolean
      -
        default: none
        description: "The background color of the section"
        label: "Background color"
        name: background_color
        options:
          - none
          - primary
          - secondary
        type: enum
      -
        description: "The image displayed in the background of the section"
        label: "Background image"
        name: background_image
        type: image
      -
        description: "An integer between 0 and 100. A lower value makes the image more transparent"
        label: "Background image opacity"
        name: background_image_opacity
        type: number
      -
        default: cover
        description: "The size of the background image"
        label: "Background image size"
        name: background_image_size
        options:
          - auto
          - contain
          - cover
        type: enum
      -
        default: "center center"
        description: "The starting position of a background image. The first value is the horizontal position, and the second value is the vertical"
        label: "Background image position"
        name: background_image_position
        options:
          - "left top"
          - "left center"
          - "left bottom"
          - "center top"
          - "center center"
          - "center bottom"
          - "right top"
          - "right center"
          - "right bottom"
        type: enum
      -
        default: no-repeat
        description: "Repeat the image to cover the whole area"
        label: "Background image repeat"
        name: background_image_repeat
        options:
          - repeat
          - no-repeat
        type: enum
    label: "CTA section"
    labelField: title
    type: object
  feature:
    fields:
      -
        description: "The title of the feature"
        label: Title
        name: title
        type: string
      -
        description: "The subtitle of the feature displayed below the title"
        label: Subtitle
        name: subtitle
        type: string
      -
        description: "The text content of the feature"
        label: Content
        name: content
        type: markdown
      -
        items:
          models:
            - action
          type: model
        label: "Action buttons"
        name: actions
        type: list
      -
        description: "The image of the feature"
        label: Image
        name: image
        type: image
      -
        description: "The alt text of the feature image"
        label: "Image alt text"
        name: image_alt
        type: string
      -
        description: "The HTML embed code for your video"
        label: "Video Embed Code"
        name: video_embed_html
        type: text
      -
        default: top
        description: "The position of the image or video relative to the text content"
        label: "Image or video position"
        name: media_position
        options:
          - left
          - right
          - top
          - bottom
        type: enum
      -
        default: fifty
        description: "The image or video container width as a percentage of the feature width. Used only when the image or video position is set to \"left\" or \"right\""
        label: "Image or video width"
        name: media_width
        options:
          - thirty-three
          - fourty
          - fifty
          - sixty
        type: enum
      -
        default: left
        description: "The horizontal alignment of the feature content"
        label: "Feature alignment"
        name: align
        options:
          - left
          - right
          - center
        type: enum
    label: Feature
    labelField: title
    type: object
  features_section:
    fields:
      -
        description: "The title of the section"
        label: Title
        name: title
        type: string
      -
        description: "The subtitle of the section displayed above the title"
        label: Subtitle
        name: subtitle
        type: string
      -
        items:
          models:
            - feature
          type: model
        label: Features
        name: features
        type: list
      -
        default: medium
        description: "The vertical spacing between section features"
        label: "Feature padding vertical"
        name: feature_padding_vert
        options:
          - small
          - medium
          - large
        type: enum
      -
        default: center
        description: "The horizontal alignment of the section content (title, subtitle)"
        label: "Section alignment"
        name: align
        options:
          - left
          - right
          - center
        type: enum
      -
        default: medium
        description: "The padding area (space) on the top of the section"
        label: "Section top padding"
        name: padding_top
        options:
          - none
          - small
          - medium
          - large
        type: enum
      -
        default: medium
        description: "The padding area (space) on the bottom of the section"
        label: "Section bottom padding"
        name: padding_bottom
        options:
          - none
          - small
          - medium
          - large
        type: enum
      -
        default: false
        description: "Add section bottom border"
        label: "Section border"
        name: has_border
        type: boolean
      -
        default: none
        description: "The background color of the section"
        label: "Background color"
        name: background_color
        options:
          - none
          - primary
          - secondary
        type: enum
      -
        description: "The image displayed in the background of the section"
        label: "Background image"
        name: background_image
        type: image
      -
        description: "An integer between 0 and 100. A lower value makes the image more transparent"
        label: "Background image opacity"
        name: background_image_opacity
        type: number
      -
        default: cover
        description: "The size of the background image"
        label: "Background image size"
        name: background_image_size
        options:
          - auto
          - contain
          - cover
        type: enum
      -
        default: "center center"
        description: "The starting position of a background image. The first value is the horizontal position, and the second value is the vertical"
        label: "Background image position"
        name: background_image_position
        options:
          - "left top"
          - "left center"
          - "left bottom"
          - "center top"
          - "center center"
          - "center bottom"
          - "right top"
          - "right center"
          - "right bottom"
        type: enum
      -
        default: no-repeat
        description: "Repeat the image to cover the whole area"
        label: "Background image repeat"
        name: background_image_repeat
        options:
          - repeat
          - no-repeat
        type: enum
    label: "Features section"
    labelField: title
    type: object
  footer:
    fields:
      -
        description: "The logo image displayed on the left side"
        label: Logo
        name: logo
        type: image
      -
        description: "The alt text of the logo image"
        label: "Logo alt text"
        name: logo_alt
        type: string
      -
        default: true
        description: "Display the primary navigation menu"
        label: "Enable primary navigation menu"
        name: has_primary_nav
        type: boolean
      -
        description: "The title of the primary navigation menu"
        label: "Primary navigation title"
        name: primary_nav_title
        type: string
      -
        description: "List of primary navigation links"
        items:
          models:
            - action
          type: model
        label: "Primary navigation menu links"
        name: primary_nav_links
        type: list
      -
        default: true
        description: "Display the secondary navigation menu"
        label: "Enable secondary navigation menu"
        name: has_secondary_nav
        type: boolean
      -
        description: "The title of the secondary navigation menu"
        label: "Secondary navigation title"
        name: secondary_nav_title
        type: string
      -
        description: "List of secondary navigation links"
        items:
          models:
            - action
          type: model
        label: "Secondary navigation menu links"
        name: secondary_nav_links
        type: list
      -
        default: true
        description: "Display the tertiary navigation menu"
        label: "Enable tertiary navigation menu"
        name: has_tertiary_nav
        type: boolean
      -
        description: "The title of the tertiary navigation menu"
        label: "Tertiary navigation title"
        name: tertiary_nav_title
        type: string
      -
        description: "List of tertiary navigation links"
        items:
          models:
            - action
          type: model
        label: "Tertiary navigation menu links"
        name: tertiary_nav_links
        type: list
      -
        default: true
        description: "Display the social links"
        label: "Enable social links"
        name: has_social
        type: boolean
      -
        description: "List of social links"
        items:
          models:
            - action
          type: model
        label: "Social links"
        name: social_links
        type: list
      -
        description: "The copyright text"
        label: "Footer content"
        name: content
        type: string
      -
        description: "A list of links displayed next to the copyright text"
        items:
          models:
            - action
          type: model
        label: Links
        name: links
        type: list
    label: "Footer configuration"
    labelField: content
    type: object
  form_field:
    fields:
      -
        description: "Type of the form field"
        label: Type
        name: input_type
        options:
          - text
          - email
          - tel
          - number
          - checkbox
          - select
          - textarea
        required: true
        type: enum
      -
        description: "The name of the field, submitted with the form"
        label: Name
        name: name
        required: true
        type: string
      -
        description: "The caption of the field, shown above the field input"
        label: Label
        name: label
        type: string
      -
        description: "The placeholder for textual field types or default option for select field"
        label: "Placeholder text or default value"
        name: default_value
        type: string
      -
        description: "The list of options, used only when the field type is \"select\""
        name: options
        type: list
      -
        default: false
        label: "Is the field required?"
        name: is_required
        type: boolean
    label: "Form field"
    labelField: name
    type: object
  form_section:
    fields:
      -
        description: "The title of the section"
        label: Title
        name: title
        type: string
      -
        description: "The subtitle of the section displayed below the title"
        label: Subtitle
        name: subtitle
        type: string
      -
        default: left
        description: "The horizontal alignment of the section title and subtitle"
        label: "Title, subtitle alignment"
        name: title_align
        options:
          - left
          - right
          - center
        type: enum
      -
        description: "The text content of the section"
        label: Content
        name: content
        type: markdown
      -
        default: left
        description: "The horizontal alignment of the section text content"
        label: "Content alignment"
        name: content_align
        options:
          - left
          - right
          - center
        type: enum
      -
        default: bottom
        description: "The position of the form relative to the text content of the section"
        label: "Form position"
        name: form_position
        options:
          - left
          - right
          - top
          - bottom
        type: enum
      -
        default: fifty
        description: "The form container width as a percentage of the section width. Used only when the form position is set to \"left\" or \"right\""
        label: "Form width"
        name: form_width
        options:
          - fourty
          - fifty
          - sixty
        type: enum
      -
        default: stacked
        description: "The layout of the form. \"Inline\" layout can only be used if the form has one input field"
        label: "Form layout"
        name: form_layout
        options:
          - stacked
          - inline
        type: enum
      -
        default: false
        description: "Display form inside a box, i.e. add border and background to the form container"
        label: "Display form in a box"
        name: enable_card
        type: boolean
      -
        description: "A unique identifier of the form. Must not contain whitespace"
        label: "Form ID"
        name: form_id
        required: true
        type: string
      -
        description: "The path of your custom \"success\" page, if you want to replace the default success message."
        label: "Form action"
        name: form_action
        type: string
      -
        items:
          models:
            - form_field
          type: model
        label: "Form fields"
        name: form_fields
        type: list
      -
        label: "Submit button label"
        name: submit_label
        required: true
        type: string
      -
        default: top
        description: "The vertical alignment of the section text content and form. Used only when the form position is set to \"left\" or \"right\""
        label: "Section vertical alignment"
        name: align_vert
        options:
          - top
          - middle
          - bottom
        type: enum
      -
        default: medium
        description: "The padding area (space) on the top of the section"
        label: "Section top padding"
        name: padding_top
        options:
          - none
          - small
          - medium
          - large
        type: enum
      -
        default: medium
        description: "The padding area (space) on the bottom of the section"
        label: "Section bottom padding"
        name: padding_bottom
        options:
          - none
          - small
          - medium
          - large
        type: enum
      -
        default: false
        description: "Add section bottom border"
        label: "Section border"
        name: has_border
        type: boolean
      -
        default: none
        description: "The background color of the section"
        label: "Background color"
        name: background_color
        options:
          - none
          - primary
          - secondary
        type: enum
      -
        description: "The image displayed in the background of the section"
        label: "Background image"
        name: background_image
        type: image
      -
        description: "An integer between 0 and 100. A lower value makes the image more transparent"
        label: "Background image opacity"
        name: background_image_opacity
        type: number
      -
        default: cover
        description: "The size of the background image"
        label: "Background image size"
        name: background_image_size
        options:
          - auto
          - contain
          - cover
        type: enum
      -
        default: "center center"
        description: "The starting position of a background image. The first value is the horizontal position, and the second value is the vertical"
        label: "Background image position"
        name: background_image_position
        options:
          - "left top"
          - "left center"
          - "left bottom"
          - "center top"
          - "center center"
          - "center bottom"
          - "right top"
          - "right center"
          - "right bottom"
        type: enum
      -
        default: no-repeat
        description: "Repeat the image to cover the whole area"
        label: "Background image repeat"
        name: background_image_repeat
        options:
          - repeat
          - no-repeat
        type: enum
    label: "Form section"
    labelField: title
    type: object
  grid_item:
    fields:
      -
        description: "The title of the item"
        label: Title
        name: title
        type: string
      -
        description: "The subtitle of the item displayed below the title"
        label: Subtitle
        name: subtitle
        type: string
      -
        default: left
        description: "The horizontal alignment of the item title and subtitle"
        label: "Title, subtitle alignment"
        name: title_align
        options:
          - left
          - right
          - center
        type: enum
      -
        description: "The text content of the item"
        label: Content
        name: content
        type: markdown
      -
        default: left
        description: "The horizontal alignment of the item text content"
        label: "Content alignment"
        name: content_align
        options:
          - left
          - right
          - center
        type: enum
      -
        items:
          models:
            - action
          type: model
        label: "Action buttons"
        name: actions
        type: list
      -
        default: left
        description: "The horizontal alignment of the item action buttons"
        label: "Action buttons alignment"
        name: actions_align
        options:
          - left
          - right
          - center
        type: enum
      -
        default: auto
        description: "The width of action button"
        label: "Action buttons width"
        name: actions_width
        options:
          - auto
          - full-width
        type: enum
      -
        description: "The image of the item"
        label: Image
        name: image
        type: image
      -
        description: "The alt text of the item image"
        label: "Image alt text"
        name: image_alt
        type: string
      -
        default: top
        description: "The position of the image relative to the text content"
        label: "Image position"
        name: image_position
        options:
          - left
          - right
          - top
          - bottom
        type: enum
      -
        default: fifty
        description: "The image container width as a percentage of the item width. Used only when the image position is set to \"left\" or \"right\""
        label: "Image container width"
        name: image_width
        options:
          - twenty-five
          - thirty-three
          - fourty
          - fifty
          - sixty
        type: enum
      -
        default: left
        description: "The horizontal alignment of the image"
        label: "Image alignment"
        name: image_align
        options:
          - left
          - right
          - center
        type: enum
      -
        default: false
        description: "Add padding (space) around the image. Used only when \"Enable cards\" is set to true"
        label: "Image padding"
        name: image_has_padding
        type: boolean
    label: Item
    labelField: title
    type: object
  grid_section:
    fields:
      -
        description: "The title of the section"
        label: Title
        name: title
        type: string
      -
        description: "The subtitle of the section displayed above the title"
        label: Subtitle
        name: subtitle
        type: string
      -
        description: "Action buttons displayed below section items"
        items:
          models:
            - action
          type: model
        label: "Action buttons"
        name: actions
        type: list
      -
        items:
          models:
            - grid_item
          type: model
        label: "Grid items"
        name: grid_items
        type: list
      -
        default: three
        description: "Show the specified number of items in a grid row"
        label: "Grid columns"
        name: grid_cols
        options:
          - two
          - three
          - four
        type: enum
      -
        default: small
        description: "The vertical spacing between grid items"
        label: "Grid gap vertical"
        name: grid_gap_vert
        options:
          - small
          - medium
          - large
        type: enum
      -
        default: small
        description: "The horizontal spacing between grid items"
        label: "Grid gap horizontal"
        name: grid_gap_horiz
        options:
          - small
          - medium
          - large
        type: enum
      -
        default: false
        description: "Display items as cards, i.e. add border, background and some padding around the item content"
        label: "Enable cards"
        name: enable_cards
        type: boolean
      -
        default: center
        description: "The horizontal alignment of the section content (title, subtitle, action buttons)"
        label: "Section alignment"
        name: align
        options:
          - left
          - right
          - center
        type: enum
      -
        default: medium
        description: "The padding area (space) on the top of the section"
        label: "Section top padding"
        name: padding_top
        options:
          - none
          - small
          - medium
          - large
        type: enum
      -
        default: medium
        description: "The padding area (space) on the bottom of the section"
        label: "Section bottom padding"
        name: padding_bottom
        options:
          - none
          - small
          - medium
          - large
        type: enum
      -
        default: false
        description: "Add section bottom border"
        label: "Section border"
        name: has_border
        type: boolean
      -
        default: none
        description: "The background color of the section"
        label: "Background color"
        name: background_color
        options:
          - none
          - primary
          - secondary
        type: enum
      -
        description: "The image displayed in the background of the section"
        label: "Background image"
        name: background_image
        type: image
      -
        description: "An integer between 0 and 100. A lower value makes the image more transparent"
        label: "Background image opacity"
        name: background_image_opacity
        type: number
      -
        default: cover
        description: "The size of the background image"
        label: "Background image size"
        name: background_image_size
        options:
          - auto
          - contain
          - cover
        type: enum
      -
        default: "center center"
        description: "The starting position of a background image. The first value is the horizontal position, and the second value is the vertical"
        label: "Background image position"
        name: background_image_position
        options:
          - "left top"
          - "left center"
          - "left bottom"
          - "center top"
          - "center center"
          - "center bottom"
          - "right top"
          - "right center"
          - "right bottom"
        type: enum
      -
        default: no-repeat
        description: "Repeat the image to cover the whole area"
        label: "Background image repeat"
        name: background_image_repeat
        options:
          - repeat
          - no-repeat
        type: enum
    label: "Grid section"
    labelField: title
    type: object
  header:
    fields:
      -
        description: "The title displayed in the header if logo image not specified"
        label: "Header title"
        name: title
        type: string
      -
        description: "The logo image displayed on the left side"
        label: Logo
        name: logo
        type: image
      -
        description: "The alt text of the logo image"
        label: "Logo alt text"
        name: logo_alt
        type: string
      -
        default: true
        description: "Display the navigation menu on the left side, next to the site title/logo"
        label: "Enable primary navigation menu"
        name: has_primary_nav
        type: boolean
      -
        description: "List of navigation links"
        items:
          models:
            - action
          type: model
        label: "Primary navigation menu links"
        name: primary_nav_links
        type: list
      -
        default: true
        description: "Display the navigation menu on the right side"
        label: "Enable secondary navigation menu"
        name: has_secondary_nav
        type: boolean
      -
        description: "List of navigation links"
        items:
          models:
            - action
          type: model
        label: "Secondary navigation menu links"
        name: secondary_nav_links
        type: list
      -
        default: false
        description: "Display the announcement above the site header"
        label: "Enable announcement"
        name: has_anncmnt
        type: boolean
      -
        description: "The text content of the announcement"
        label: "Announcement content"
        name: anncmnt_content
        type: markdown
      -
        default: left
        description: "The horizontal alignment of the announcement content"
        label: "Announcement horizontal alignment"
        name: anncmnt_align
        options:
          - left
          - right
          - center
        type: enum
      -
        default: false
        description: "Display the announcement on the home page only."
        label: "Show on home page"
        name: anncmnt_is_home_only
        type: boolean
      -
        default: false
        description: "Display the announcement close button"
        label: "Enable announcement close"
        name: anncmnt_has_close
        type: boolean
      -
        description: "A unique identifier, required if close button is enabled. Must be updated each time the announcement content changes"
        label: "Announcement ID"
        name: anncmnt_id
        type: string
    label: "Header configuration"
    labelField: title
    type: object
  hero_section:
    fields:
      -
        description: "The title of the section"
        label: Title
        name: title
        type: string
      -
        description: "The subtitle of the section displayed below the title"
        label: Subtitle
        name: subtitle
        type: string
      -
        description: "The text content of the section"
        label: Content
        name: content
        type: markdown
      -
        items:
          models:
            - action
          type: model
        label: "Action buttons"
        name: actions
        type: list
      -
        description: "The image of the section"
        label: Image
        name: image
        type: image
      -
        description: "The alt text of the section image"
        label: "Image alt text"
        name: image_alt
        type: string
      -
        description: "The HTML embed code for your video"
        label: "Video Embed Code"
        name: video_embed_html
        type: text
      -
        default: top
        description: "The position of the image or video relative to the text content"
        label: "Image or video position"
        name: media_position
        options:
          - left
          - right
          - top
          - bottom
        type: enum
      -
        default: fifty
        description: "The image or video container width as a percentage of the section width. Used only when the image or video position is set to \"left\" or \"right\""
        label: "Image or video width"
        name: media_width
        options:
          - fourty
          - fifty
          - sixty
        type: enum
      -
        default: left
        description: "The horizontal alignment of the section content"
        label: "Section alignment"
        name: align
        options:
          - left
          - right
          - center
        type: enum
      -
        default: medium
        description: "The padding area (space) on the top of the section"
        label: "Section top padding"
        name: padding_top
        options:
          - none
          - small
          - medium
          - large
        type: enum
      -
        default: medium
        description: "The padding area (space) on the bottom of the section"
        label: "Section bottom padding"
        name: padding_bottom
        options:
          - none
          - small
          - medium
          - large
        type: enum
      -
        default: false
        description: "Add section bottom border"
        label: "Section border"
        name: has_border
        type: boolean
      -
        default: none
        description: "The background color of the section"
        label: "Background color"
        name: background_color
        options:
          - none
          - primary
          - secondary
        type: enum
      -
        description: "The image displayed in the background of the section"
        label: "Background image"
        name: background_image
        type: image
      -
        description: "An integer between 0 and 100. A lower value makes the image more transparent"
        label: "Background image opacity"
        name: background_image_opacity
        type: number
      -
        default: cover
        description: "The size of the background image"
        label: "Background image size"
        name: background_image_size
        options:
          - auto
          - contain
          - cover
        type: enum
      -
        default: "center center"
        description: "The starting position of a background image. The first value is the horizontal position, and the second value is the vertical"
        label: "Background image position"
        name: background_image_position
        options:
          - "left top"
          - "left center"
          - "left bottom"
          - "center top"
          - "center center"
          - "center bottom"
          - "right top"
          - "right center"
          - "right bottom"
        type: enum
      -
        default: no-repeat
        description: "Repeat the image to cover the whole area"
        label: "Background image repeat"
        name: background_image_repeat
        options:
          - repeat
          - no-repeat
        type: enum
    label: "Hero section"
    labelField: title
    type: object
  page:
    exclude: blog/**/*
    fields:
      -
        description: "The title of the page"
        label: Title
        name: title
        required: true
        type: string
      -
        models:
          - stackbit_page_meta
        name: seo
        type: model
    label: Page
    layout: page
    type: page
  person:
    fields:
      -
        description: "A unique identifier used when filtering posts, e.g. \"john-doe\""
        label: ID
        name: id
        required: true
        type: string
      -
        description: "The link to the author page, e.g. \"blog/author/john-doe\""
        label: Link
        name: link
        type: string
      -
        label: "First name"
        name: first_name
        required: true
        type: string
      -
        label: "Last name"
        name: last_name
        type: string
      -
        label: Image
        name: photo
        type: image
      -
        label: "Image alt text"
        name: photo_alt
        type: string
    folder: team
    label: Person
    type: data
  post:
    exclude:
      - index.md
      - category/**
      - author/**
      - tag/**
    fields:
      -
        description: "The title of the post"
        label: Title
        name: title
        required: true
        type: string
      -
        description: "The subtitle of the post"
        label: Subtitle
        name: subtitle
        type: string
      -
        description: "The publish date of the post"
        label: Date
        name: date
        required: true
        type: date
      -
        description: "The author of the post"
        label: Author
        models:
          - person
        name: author
        type: reference
      -
        description: "The categories of the post"
        items:
          models:
            - category
          type: reference
        label: Categories
        name: categories
        type: list
      -
        description: "The tags of the post"
        items:
          models:
            - tag
          type: reference
        label: Tags
        name: tags
        type: list
      -
        description: "The excerpt of the post displayed in the blog feed"
        label: Excerpt
        name: excerpt
        type: string
      -
        description: "The image displayed in the blog feed"
        label: "Image (blog feed)"
        name: thumb_image
        type: image
      -
        description: "The alt text of the blog feed image"
        label: "Image alt text (blog feed)"
        name: thumb_image_alt
        type: string
      -
        description: "The image displayed in the single post"
        label: "Image (single post)"
        name: image
        type: image
      -
        description: "The alt text of the single post image"
        label: "Image alt text (single post)"
        name: image_alt
        type: string
      -
        default: top
        description: "The featured image position in the blog post relative to the post header content"
        label: "Image position (single post)"
        name: image_position
        options:
          - left
          - right
          - top
        type: enum
      -
        models:
          - stackbit_page_meta
        name: seo
        type: model
    folder: blog
    label: Post
    layout: post
    type: page
    urlPath: "/blog/{slug}"
  stackbit_page_meta:
    fields:
      -
        default: ""
        description: "The page title that goes into the <title> tag"
        label: Title
        name: title
        type: string
      -
        default: ""
        description: "The page description that goes into the <meta name=\"description\"> tag"
        label: Description
        name: description
        type: string
      -
        description: "The items that go into the <meta name=\"robots\"> tag"
        items:
          options:
            - all
            - index
            - follow
            - noindex
            - nofollow
            - noimageindex
            - notranslate
            - none
          type: enum
        label: Robots
        name: robots
        type: list
      -
        description: "Additional definition for specific meta tags such as open-graph, twitter, etc."
        items:
          fields:
            -
              default: ""
              name: name
              type: string
            -
              default: ""
              name: value
              type: string
            -
              default: name
              name: keyName
              type: string
            -
              name: relativeUrl
              type: boolean
          labelField: name
          type: object
        label: Extra
        name: extra
        type: list
    label: "Page meta data"
    type: object
  tag:
    fields:
      -
        description: "A unique identifier used when filtering posts, e.g. \"jamstack\""
        label: ID
        name: id
        required: true
        type: string
      -
        description: "The link to the tag page, e.g. \"blog/tag/jamstack\""
        label: Link
        name: link
        type: string
      -
        description: "The title of the tag"
        label: Title
        name: title
        required: true
        type: string
    folder: tags
    label: Tag
    type: data
nodeVersion: "14.16.1"
pageLayoutKey: layout
pagesDir: content/pages
publishDir: out
ssgName: nextjs
ssgVersion: "10.2.0"
stackbitVersion: ~0.3.0
staticDir: public
uploadDir: images
@endyaml
```

## How to use

Execute
[`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app) with
[npm](https://docs.npmjs.com/cli/init) or [Yarn](https://yarnpkg.com/lang/en/docs/cli/create/)
to bootstrap the example:

```bash
npx create-next-app --example with-zones with-zones-app
# or
yarn create next-app --example with-zones with-zones-app
```

With multi zones you have multiple Next.js apps over a single app, therefore every app has its
own dependencies and it runs independently.

To start the `/home` run the following commands from the root directory:

```bash
cd home
npm install && npm run dev
# or
cd home
yarn && yarn dev
```

The `/home` app should be up and running in [http://localhost:3000](http://localhost:3000)!

Starting the `/blog` app follows a very similar process. In a new terminal, run the following
commands from the root directory :

```bash
cd blog
npm install && npm run dev
# or
cd blog
yarn && yarn dev
```

The `blog` app should be up and running in [http://localhost:4000](http://localhost:4000)!

### Deploy on Vercel

You can deploy this app to the cloud with
[Vercel](https://vercel.com?utm_source=github&utm_medium=readme&utm_campaign=next-example)
([Documentation](https://nextjs.org/docs/deployment)).

#### Deploy Your Local Project

To deploy the apps to Vercel, we'll use [monorepos support](https://vercel.com/blog/monorepos)
to create a new project for each app.

To get started, push the example to GitHub/GitLab/Bitbucket and
[import your repo to Vercel](https://vercel.com/new?utm_source=github&utm_medium=readme&utm_campaign=next-example).
We're not interested in the root directory, so make sure to select the `blog` directory (do not
start with `home`):

![Import flow for blog app](docs/import-blog.jpg)

Click continue and finish the import process. After that's done copy the domain URL that was
assigned to your project, paste it on `home/.env`, and push the change to your repo:

```bash
# Replace this URL with the URL of your blog app
BLOG_URL="https://with-zones-blog.vercel.app"
```

Now we'll go over the
[import flow](https://vercel.com/new?utm_source=github&utm_medium=readme&utm_campaign=next-example)
again using the same repo but this time select the `home` directory instead:

![Import flow for home app](docs/import-home.jpg)

With the `home` app deployed you should now be able to see both apps running under the same
domain!

Any future commits to the repo will trigger a deployment to the connected Vercel projects. See
the [blog post about monorepos](https://vercel.com/blog/monorepos) to learn more.
